# Jetson Notebook GitLab Repository
> Aim: Storage & Sync of Jetson Nano - User: Jetbot/Notebook 

---------------
## Upon Kickstart - Git Usage
> Reference at [Gitlab](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
```
Run Gitlab Cloud.ipynb
```
## Prerequisite 

------------
## Instruction Memory Bay
- Translate .ipynb to .py
```bash
$ jupyter nbconvert --to script *.ipynb
```

- SSH Jupyter Tunneling
```bash
# ssh -N -L <local_port>:localhost:<remote_port> <remote_user>@<remote_host>
$ ssh -L 8080:localhost:8888 jetbot@<ip_address>

